﻿namespace Rey.Logging {
    public interface ILogFilter {
        bool Filter(ILogData data);
    }
}
