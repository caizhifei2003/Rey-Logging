﻿namespace Rey.Logging {
    public enum LogLevel {
        Trace = 1,
        Info,
        Debug,
        Warn,
        Error,
    }
}
