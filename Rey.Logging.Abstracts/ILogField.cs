﻿namespace Rey.Logging {
    public interface ILogField {
        string Name { get; }
        object Value { get; }
    }
}
