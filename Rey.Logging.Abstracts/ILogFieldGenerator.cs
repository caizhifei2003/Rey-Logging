﻿namespace Rey.Logging {
    public interface ILogFieldGenerator {
        ILogField Generate(ILogData data, ILogStack stack);
    }
}
