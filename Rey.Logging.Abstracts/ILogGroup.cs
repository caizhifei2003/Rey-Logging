﻿using System;

namespace Rey.Logging {
    public interface ILogGroup<T> : IDisposable
        where T : class {
        ILogGroup<T> Log(ILogData data);
    }
}
