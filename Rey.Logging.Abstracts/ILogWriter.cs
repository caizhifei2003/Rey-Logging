﻿using System.Threading.Tasks;

namespace Rey.Logging {
    public interface ILogWriter {
        Task WriteAsync(ILogData data);
    }
}
