﻿namespace Rey.Logging {
    public interface ILogger<T>
        where T : class {
        ILogger<T> Log(ILogData data);
        ILogGroup<T> Group(string name);
    }
}
