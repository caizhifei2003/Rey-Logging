﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rey.Logging {
    public interface ILogGroupWriter {
        Task WriteAsync(string name, IEnumerable<ILogData> dataList);
    }
}
