﻿using System.Collections.Generic;

namespace Rey.Logging {
    public interface ILogData {
        LogLevel Level { get; }
        IEnumerable<ILogField> Fields { get; }

        ILogData Append(ILogField field);
        ILogData Append(IEnumerable<ILogField> fields);
        ILogData Prepend(ILogField field);
        ILogData Prepend(IEnumerable<ILogField> fields);
    }
}
