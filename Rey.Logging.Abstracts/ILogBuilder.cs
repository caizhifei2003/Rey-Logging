﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Rey.Logging {
    public interface ILogBuilder {
        ILogBuilder Use(Action<IServiceCollection> configure);

        ILogBuilder Filter<T>()
            where T : class, ILogFilter;

        ILogBuilder Field<T>()
            where T : class, ILogFieldGenerator;
    }
}
