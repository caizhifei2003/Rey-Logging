﻿using System;
using System.Reflection;

namespace Rey.Logging {
    public interface ILogStack {
        string GetFileName();
        int GetLine();
        int GetColumn();
        MethodBase GetMethod();
        Type GetClass();
    }
}
