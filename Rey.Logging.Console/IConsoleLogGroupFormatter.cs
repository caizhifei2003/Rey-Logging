﻿namespace Rey.Logging.Console {
    public interface IConsoleLogGroupFormatter {
        string FormatGroupBegin(string name);
        string FormatGroupEnd(string name);
        string FormatGroup(ILogData data);
    }
}
