﻿namespace Rey.Logging.Console {
    public interface IConsoleLogBuilder {
        IConsoleLogBuilder Formatter<T>()
            where T : class, IConsoleLogFormatter;

        IConsoleLogBuilder GroupFormatter<T>()
            where T : class, IConsoleLogGroupFormatter;

        IConsoleLogBuilder Color<T>()
            where T : class, IConsoleLogColor;
    }
}
