﻿using System;

namespace Rey.Logging.Console {
    public interface IConsoleLogColor {
        ConsoleColor? Color(ILogData data);
    }
}
