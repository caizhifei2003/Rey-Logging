﻿using System;
using System.Text;

namespace Rey.Logging.Console {
    public class ConsoleLogFormatter : IConsoleLogFormatter, IConsoleLogGroupFormatter {
        public static string GROUP_NAME_PREFIX = new string('-', 30);
        public static string GROUP_NAME_POST = new string('-', 30);

        public string Format(ILogData data) {
            if (data == null)
                throw new ArgumentNullException(nameof(data));

            var builder = new StringBuilder();
            foreach (var field in data.Fields) {
                var name = field.Name == null ? "" : $"{field.Name}: ";
                builder.Append($"[{name}{field.Value}]");
            }
            builder.AppendLine();
            return builder.ToString();
        }

        public string FormatGroupBegin(string name) {
            if (name == null)
                throw new ArgumentNullException(nameof(name));

            var builder = new StringBuilder();
            builder.Append($"{GROUP_NAME_PREFIX}Begin {name}{GROUP_NAME_POST}");
            builder.AppendLine();
            return builder.ToString();
        }

        public string FormatGroupEnd(string name) {
            if (name == null)
                throw new ArgumentNullException(nameof(name));

            var builder = new StringBuilder();
            builder.Append($"{GROUP_NAME_PREFIX}  End {name}{GROUP_NAME_POST}");
            builder.AppendLine();
            return builder.ToString();
        }

        public string FormatGroup(ILogData data) {
            if (data == null)
                throw new ArgumentNullException(nameof(data));

            return this.Format(data);
        }
    }
}
