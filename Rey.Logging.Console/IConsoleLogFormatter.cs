﻿namespace Rey.Logging.Console {
    public interface IConsoleLogFormatter {
        string Format(ILogData data);
    }
}
