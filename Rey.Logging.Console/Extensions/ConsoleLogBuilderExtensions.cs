﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Rey.Logging;
using Rey.Logging.Console;
using System;

namespace Rey {
    public static class ConsoleLogBuilderExtensions {
        public static ILogBuilder UseConsole(this ILogBuilder builder, Action<IConsoleLogBuilder> configure = null) {
            return builder
                .Use(services => {
                    configure?.Invoke(new ConsoleLogBuilder(services));
                    services.AddSingleton<ILogWriter, ConsoleLogWriter>();
                    services.AddSingleton<ILogGroupWriter, ConsoleLogWriter>();
                    services.TryAddTransient<IConsoleLogFormatter, ConsoleLogFormatter>();
                    services.TryAddTransient<IConsoleLogGroupFormatter, ConsoleLogFormatter>();
                    services.TryAddTransient<IConsoleLogColor, ConsoleLogColor>();
                });
        }
    }
}
