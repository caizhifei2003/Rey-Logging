﻿using Microsoft.Extensions.DependencyInjection;

namespace Rey.Logging.Console {
    public class ConsoleLogBuilder : IConsoleLogBuilder {
        private IServiceCollection Services { get; }

        public ConsoleLogBuilder(IServiceCollection services) {
            this.Services = services;
        }

        public IConsoleLogBuilder Formatter<T>()
            where T : class, IConsoleLogFormatter {
            this.Services.AddTransient<IConsoleLogFormatter, T>();
            return this;
        }

        public IConsoleLogBuilder GroupFormatter<T>()
            where T : class, IConsoleLogGroupFormatter {
            this.Services.AddTransient<IConsoleLogGroupFormatter, T>();
            return this;
        }

        public IConsoleLogBuilder Color<T>()
            where T : class, IConsoleLogColor {
            this.Services.AddTransient<IConsoleLogColor, T>();
            return this;
        }
    }
}
