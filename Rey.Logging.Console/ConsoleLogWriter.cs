﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rey.Logging.Console {
    public class ConsoleLogWriter : ILogWriter, ILogGroupWriter {
        private IConsoleLogFormatter Formatter { get; }
        private IConsoleLogGroupFormatter GroupFormatter { get; }
        private IConsoleLogColor Color { get; }

        public ConsoleLogWriter(IConsoleLogFormatter formatter, IConsoleLogGroupFormatter groupFormatter, IConsoleLogColor color) {
            this.Formatter = formatter;
            this.GroupFormatter = groupFormatter;
            this.Color = color;
        }

        public Task WriteAsync(ILogData data) {
            if (data == null)
                throw new ArgumentNullException(nameof(data));

            var content = this.Formatter.Format(data);
            using (new ForeColor(this.Color?.Color(data))) {
                System.Console.Write(content);
            }
            return Task.CompletedTask;
        }

        public Task WriteAsync(string name, IEnumerable<ILogData> dataList) {
            System.Console.Write(this.GroupFormatter.FormatGroupBegin(name));
            foreach (var data in dataList) {
                var content = this.GroupFormatter.FormatGroup(data);
                using (new ForeColor(this.Color?.Color(data))) {
                    System.Console.Write(content);
                }
            }
            System.Console.Write(this.GroupFormatter.FormatGroupEnd(name));
            return Task.CompletedTask;
        }
    }

    internal class ForeColor : IDisposable {
        private ConsoleColor? Color { get; }
        public ForeColor(ConsoleColor? color) {
            if (color != null) {
                this.Color = System.Console.ForegroundColor;
                System.Console.ForegroundColor = color.Value;
            }
        }

        public void Dispose() {
            if (this.Color != null) {
                System.Console.ForegroundColor = this.Color.Value;
            }
        }
    }
}
