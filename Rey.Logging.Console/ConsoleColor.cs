﻿using System;

namespace Rey.Logging.Console {
    public class ConsoleLogColor : IConsoleLogColor {
        public ConsoleColor? Color(ILogData data) {
            switch (data.Level) {
                case LogLevel.Trace:
                case LogLevel.Info:
                case LogLevel.Debug:
                    break;
                case LogLevel.Warn:
                    return ConsoleColor.Yellow;
                case LogLevel.Error:
                    return ConsoleColor.Red;
                default:
                    break;
            }
            return null;
        }
    }
}
