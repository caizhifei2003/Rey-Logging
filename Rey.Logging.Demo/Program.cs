﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rey.Logging.Demo {
    class Program {
        static void Main(string[] args) {
            new ServiceCollection()
                .AddTransient<App>()
                .AddLogging(options => {
                    options.AllFields();
                    options.UseConsole();
                    options.UseFile();
                })
                .BuildServiceProvider()
                .GetService<App>()
                .Test();

            System.Console.ReadLine();
        }
    }

    public class MyFilter : ILogFilter {
        public bool Filter(ILogData data) {
            return true;
        }
    }

    public class App {
        private ILogger<App> Logger { get; }
        public App(ILogger<App> logger) {
            this.Logger = logger;
        }

        public void Test() {
            var fields = new List<ILogField>() {
                new LogField("name 1", "value 1"),
                new LogField("name 2", "value 2")
            };
            var data = new LogData(LogLevel.Trace, fields);
            Task.Run(() => {
                Task.Run(() => {
                    this.Logger.Log(data);
                    this.Logger.Log(LogLevel.Info, fields);
                    this.Logger.Log(LogLevel.Debug, new LogField("name 1", "value 1"), new LogField("name 2", "value 2"));
                    this.Logger.Log(LogLevel.Warn, ("name 1", "value 1"), ("name 2", "value 2"));
                    this.Logger.Log(LogLevel.Error, ("name 1", "value 1"));
                }).Wait();
            }).Wait();

            this.Logger.Log(data);
            this.Logger.Log(LogLevel.Info, fields);
            this.Logger.Log(LogLevel.Debug, new LogField("name 1", "value 1"), new LogField("name 2", "value 2"));
            this.Logger.Log(LogLevel.Warn, ("name 1", "value 1"), ("name 2", "value 2"));
            this.Logger.Log(LogLevel.Error, ("name 1", "value 1"));

            this.Logger.Info(fields);
            this.Logger.Info(new LogField("name 1", "value 1"), new LogField("name 2", "value 2"));
            this.Logger.Info(("name 1", "value 1"), ("name 2", "value 2"));
            this.Logger.Info(("name 1", "value 1"));
            this.Logger.Error("message", new Exception("unknown exception"));

            using (var group = this.Logger.Group("group name")) {
                Task.Delay(1000).Wait();
                group.Log(data);
                group.Log(LogLevel.Info, fields);
                group.Log(LogLevel.Debug, new LogField("name 1", "value 1"), new LogField("name 2", "value 2"));
                group.Log(LogLevel.Warn, ("name 1", "value 1"), ("name 2", "value 2"));
                group.Log(LogLevel.Error, ("name 1", "value 1"));

                Task.Delay(1000).Wait();
                group.Info(fields);
                group.Info(new LogField("name 1", "value 1"), new LogField("name 2", "value 2"));
                group.Info(("name 1", "value 1"), ("name 2", "value 2"));
                group.Info(("name 1", "value 1"));
                group.Error("message", new Exception("unknown exception"));
            }
        }
    }
}
