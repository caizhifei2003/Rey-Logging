﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Rey.Logging.File {
    public class FileLogFormatter : IFileLogFormatter, IFileLogGroupFormatter, IFileLogPathFormatter {
        public static string GROUP_NAME_PREFIX = new string('-', 30);
        public static string GROUP_NAME_POST = new string('-', 30);

        private IFileLogOptions Options { get; }

        public FileLogFormatter(IFileLogOptions options) {
            this.Options = options;
        }

        public string Format(ILogData data) {
            if (data == null)
                throw new ArgumentNullException(nameof(data));

            var builder = new StringBuilder();
            foreach (var field in data.Fields) {
                var name = field.Name == null ? "" : $"{field.Name}: ";
                builder.Append($"[{name}{field.Value}]");
            }
            builder.AppendLine();
            return builder.ToString();
        }

        public string FormatGroupBegin(string name) {
            if (name == null)
                throw new ArgumentNullException(nameof(name));

            var builder = new StringBuilder();
            builder.Append($"{GROUP_NAME_PREFIX}Begin {name}{GROUP_NAME_POST}");
            builder.AppendLine();
            return builder.ToString();
        }

        public string FormatGroupEnd(string name) {
            if (name == null)
                throw new ArgumentNullException(nameof(name));

            var builder = new StringBuilder();
            builder.Append($"{GROUP_NAME_PREFIX}  End {name}{GROUP_NAME_POST}");
            builder.AppendLine();
            return builder.ToString();
        }

        public string FormatGroup(ILogData data) {
            if (data == null)
                throw new ArgumentNullException(nameof(data));

            return this.Format(data);
        }

        public string FormatPath(string name) {
            var path = name;
            path = path.Replace('\\', '/');
            path = ReplaceDate(path);

            var absolute = GetAbsolutePath(ReplaceIndex(path, 0));
            if (!System.IO.File.Exists(absolute)
               || new FileInfo(absolute).Length <= this.Options.Size) {
                return absolute;
            }

            var pattern = Regex.Replace(path, @"\{(?<var>index:?(?<len>\d+)?)\}", m => {
                var len = m.Groups["len"];
                if (len.Success) {
                    return @"(?<index>\d{" + len.Value + "})";
                }
                return @"(?<index>\d+)";
            });

            var max = 0;
            var files = Directory.GetFiles(Path.GetDirectoryName(absolute));
            foreach (var file in files) {
                var match = Regex.Match(file.Replace('\\', '/'), pattern);
                if (match.Success) {
                    var index = int.Parse(match.Groups["index"].Value);
                    if (index > max) {
                        max = index;
                    }
                }
            }

            var dest = GetAbsolutePath(ReplaceIndex(path, max + 1));
            System.IO.File.Move(absolute, dest);
            return absolute;
        }

        private static string ReplaceDate(string path) {
            return Regex.Replace(path, @"\{date:(?<var>.*?)\}", m =>
                DateTime.Now.ToString(m.Groups["var"].Value)
            );
        }

        private static string ReplaceIndex(string path, int? index = null) {
            return Regex.Replace(path, @"\{(?<var>index:?(?<len>\d+)?)\}", m => {
                if (index == null)
                    return string.Empty;

                var len = m.Groups["len"];
                if (len.Success) {
                    return string.Format($"{{0:D{len.Value}}}", index);
                }
                return $"{index}";
            });
        }

        private static string GetAbsolutePath(string path) {
            var absolute = path;
            absolute = absolute.Replace('/', Path.DirectorySeparatorChar);

            if (!Path.IsPathRooted(absolute)) {
                var entryDir = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                absolute = Path.Combine(entryDir, absolute);
            }

            var dir = Path.GetDirectoryName(absolute);
            if (!Directory.Exists(dir)) {
                Directory.CreateDirectory(dir);
            }

            return absolute;
        }
    }
}
