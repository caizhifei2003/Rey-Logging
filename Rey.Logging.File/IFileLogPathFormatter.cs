﻿namespace Rey.Logging.File {
    public interface IFileLogPathFormatter {
        string FormatPath(string name);
    }
}
