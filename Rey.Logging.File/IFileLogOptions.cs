﻿using System.Text;

namespace Rey.Logging.File {
    public interface IFileLogOptions {
        string Path { get; }
        long Size { get; }
        Encoding Encoding { get; }

        IFileLogOptions Formatter<T>()
            where T : class, IFileLogFormatter;

        IFileLogOptions GroupFormatter<T>()
            where T : class, IFileLogGroupFormatter;
    }
}
