﻿namespace Rey.Logging.File {
    public interface IFileLogFormatter {
        string Format(ILogData data);
    }
}
