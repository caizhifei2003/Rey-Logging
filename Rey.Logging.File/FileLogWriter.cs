﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Rey.Logging.File {
    public class FileLogWriter : ILogWriter, ILogGroupWriter {
        private IFileLogOptions Options { get; }
        private IFileLogFormatter Formatter { get; }
        private IFileLogGroupFormatter GroupFormatter { get; }
        private IFileLogPathFormatter PathFormatter { get; }

        public FileLogWriter(
            IFileLogOptions options,
            IFileLogFormatter formatter,
            IFileLogGroupFormatter groupFormatter,
            IFileLogPathFormatter pathFormatter) {
            this.Options = options;
            this.Formatter = formatter;
            this.GroupFormatter = groupFormatter;
            this.PathFormatter = pathFormatter;
        }

        public async Task WriteAsync(ILogData data) {
            if (data == null)
                throw new ArgumentNullException(nameof(data));

            var content = this.Formatter.Format(data);
            if (string.IsNullOrEmpty(content))
                return;

            using (var output = System.IO.File.OpenWrite(this.PathFormatter.FormatPath(this.Options.Path))) {
                output.Seek(0, SeekOrigin.End);
                using (var writer = new StreamWriter(output, this.Options.Encoding)) {
                    await writer.WriteAsync(content);
                }
            }
        }

        public async Task WriteAsync(string name, IEnumerable<ILogData> dataList) {
            if (string.IsNullOrEmpty(name) || dataList == null || dataList.Count() == 0)
                return;

            using (var output = System.IO.File.OpenWrite(this.PathFormatter.FormatPath(this.Options.Path))) {
                output.Seek(0, SeekOrigin.End);
                using (var writer = new StreamWriter(output, this.Options.Encoding)) {
                    await writer.WriteAsync(this.GroupFormatter.FormatGroupBegin(name));
                    foreach (var data in dataList) {
                        var content = this.GroupFormatter.FormatGroup(data);
                        await writer.WriteAsync(this.GroupFormatter.FormatGroup(data));
                    }
                    await writer.WriteAsync(this.GroupFormatter.FormatGroupEnd(name));
                }
            }
        }
    }
}
