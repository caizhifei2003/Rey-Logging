﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Rey.Logging;
using Rey.Logging.File;
using System;

namespace Rey {
    public static class FileLogBuilderExtensions {
        public static ILogBuilder UseFile(this ILogBuilder builder, Action<FileLogOptions> configure = null) {
            return builder
                .Use(services => {
                    var options = new FileLogOptions(services);
                    configure?.Invoke(options);
                    services.AddSingleton<IFileLogOptions>(options);
                    services.AddSingleton<ILogWriter, FileLogWriter>();
                    services.AddSingleton<ILogGroupWriter, FileLogWriter>();
                    services.TryAddTransient<IFileLogFormatter, FileLogFormatter>();
                    services.TryAddTransient<IFileLogGroupFormatter, FileLogFormatter>();
                    services.TryAddTransient<IFileLogPathFormatter, FileLogFormatter>();
                });
        }
    }
}
