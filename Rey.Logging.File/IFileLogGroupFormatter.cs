﻿namespace Rey.Logging.File {
    public interface IFileLogGroupFormatter {
        string FormatGroupBegin(string name);
        string FormatGroupEnd(string name);
        string FormatGroup(ILogData data);
    }
}
