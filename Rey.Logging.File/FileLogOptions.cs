﻿using Microsoft.Extensions.DependencyInjection;
using System.Text;

namespace Rey.Logging.File {
    public class FileLogOptions : IFileLogOptions {
        public const long ONE_KB = 1024;
        public const long ONE_MB = ONE_KB * 1024;
        public const long TEN_MB = ONE_MB * 10;

        private IServiceCollection Services { get; }

        public string Path { get; set; } = "logs/{date:yyyy-MM-dd}/{index}.log";
        public long Size { get; set; } = ONE_MB;
        public Encoding Encoding { get; set; } = Encoding.UTF8;

        public FileLogOptions(IServiceCollection services) {
            this.Services = services;
        }

        public IFileLogOptions Formatter<T>()
            where T : class, IFileLogFormatter {
            this.Services.AddTransient<IFileLogFormatter, T>();
            return this;
        }

        public IFileLogOptions GroupFormatter<T>()
            where T : class, IFileLogGroupFormatter {
            this.Services.AddTransient<IFileLogGroupFormatter, T>();
            return this;
        }
    }
}
