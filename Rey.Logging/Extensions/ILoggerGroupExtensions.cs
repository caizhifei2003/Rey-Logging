﻿using Rey.Logging;
using System;
using System.Collections.Generic;

namespace Rey {
    public static class ILoggerGroupExtensions {
        public static ILogGroup<T> Log<T>(this ILogGroup<T> group, LogLevel level, IEnumerable<ILogField> fields)
            where T : class {
            return group.Log(LogData.Create(level, fields));
        }

        public static ILogGroup<T> Log<T>(this ILogGroup<T> group, LogLevel level, ILogField field, params ILogField[] fields)
            where T : class {
            return group.Log(LogData.Create(level, field, fields));
        }

        public static ILogGroup<T> Log<T>(this ILogGroup<T> group, LogLevel level, (string name, object value) field, params (string name, object value)[] fields)
            where T : class {
            return group.Log(LogData.Create(level, field, fields));
        }

        #region

        public static ILogGroup<T> Trace<T>(this ILogGroup<T> group, IEnumerable<ILogField> fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Trace, fields));
        }

        public static ILogGroup<T> Info<T>(this ILogGroup<T> group, IEnumerable<ILogField> fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Info, fields));
        }

        public static ILogGroup<T> Debug<T>(this ILogGroup<T> group, IEnumerable<ILogField> fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Debug, fields));
        }

        public static ILogGroup<T> Warn<T>(this ILogGroup<T> group, IEnumerable<ILogField> fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Warn, fields));
        }

        public static ILogGroup<T> Error<T>(this ILogGroup<T> group, IEnumerable<ILogField> fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Error, fields));
        }

        #endregion

        #region

        public static ILogGroup<T> Trace<T>(this ILogGroup<T> group, ILogField field, params ILogField[] fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Trace, field, fields));
        }

        public static ILogGroup<T> Info<T>(this ILogGroup<T> group, ILogField field, params ILogField[] fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Info, field, fields));
        }

        public static ILogGroup<T> Debug<T>(this ILogGroup<T> group, ILogField field, params ILogField[] fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Debug, field, fields));
        }

        public static ILogGroup<T> Warn<T>(this ILogGroup<T> group, ILogField field, params ILogField[] fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Warn, field, fields));
        }

        public static ILogGroup<T> Error<T>(this ILogGroup<T> group, ILogField field, params ILogField[] fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Error, field, fields));
        }

        #endregion

        #region

        public static ILogGroup<T> Trace<T>(this ILogGroup<T> group, (string name, object value) field, params (string name, object value)[] fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Trace, field, fields));
        }

        public static ILogGroup<T> Info<T>(this ILogGroup<T> group, (string name, object value) field, params (string name, object value)[] fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Info, field, fields));
        }

        public static ILogGroup<T> Debug<T>(this ILogGroup<T> group, (string name, object value) field, params (string name, object value)[] fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Debug, field, fields));
        }

        public static ILogGroup<T> Warn<T>(this ILogGroup<T> group, (string name, object value) field, params (string name, object value)[] fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Warn, field, fields));
        }

        public static ILogGroup<T> Error<T>(this ILogGroup<T> group, (string name, object value) field, params (string name, object value)[] fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Error, field, fields));
        }

        #endregion

        #region

        public static ILogGroup<T> Trace<T>(this ILogGroup<T> group, string msg, params (string name, object value)[] fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Trace, ("msg", msg), fields));
        }

        public static ILogGroup<T> Info<T>(this ILogGroup<T> group, string msg, params (string name, object value)[] fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Info, ("msg", msg), fields));
        }

        public static ILogGroup<T> Debug<T>(this ILogGroup<T> group, string msg, params (string name, object value)[] fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Debug, ("msg", msg), fields));
        }

        public static ILogGroup<T> Warn<T>(this ILogGroup<T> group, string msg, params (string name, object value)[] fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Warn, ("msg", msg), fields));
        }

        public static ILogGroup<T> Error<T>(this ILogGroup<T> group, string msg, params (string name, object value)[] fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Error, ("msg", msg), fields));
        }

        #endregion

        #region

        public static ILogGroup<T> Trace<T>(this ILogGroup<T> group, string msg, Exception ex, params (string name, object value)[] fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Trace, ("msg", msg), ("error", ex.Message), fields));
        }

        public static ILogGroup<T> Info<T>(this ILogGroup<T> group, string msg, Exception ex, params (string name, object value)[] fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Info, ("msg", msg), ("error", ex.Message), fields));
        }

        public static ILogGroup<T> Debug<T>(this ILogGroup<T> group, string msg, Exception ex, params (string name, object value)[] fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Debug, ("msg", msg), ("error", ex.Message), fields));
        }

        public static ILogGroup<T> Warn<T>(this ILogGroup<T> group, string msg, Exception ex, params (string name, object value)[] fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Warn, ("msg", msg), ("error", ex.Message), fields));
        }

        public static ILogGroup<T> Error<T>(this ILogGroup<T> group, string msg, Exception ex, params (string name, object value)[] fields)
            where T : class {
            return group.Log(LogData.Create(LogLevel.Error, ("msg", msg), ("error", ex.Message), fields));
        }

        #endregion
    }
}
