﻿using Microsoft.Extensions.DependencyInjection;
using Rey.Logging;
using System;

namespace Rey {
    public static class LoggingServiceCollectionExtensions {
        public static IServiceCollection AddLogging(this IServiceCollection services, Action<ILogBuilder> configure = null) {
            var builder = new LogBuilder(services);
            configure?.Invoke(builder);
            services.AddTransient(typeof(ILogger<>), typeof(Logger<>));
            return services;
        }
    }
}
