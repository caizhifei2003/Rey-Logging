﻿using Rey.Logging;
using System;
using System.Collections.Generic;

namespace Rey {
    public static class ILoggerExtensions {
        public static ILogger<T> Log<T>(this ILogger<T> logger, LogLevel level, IEnumerable<ILogField> fields)
            where T : class {
            return logger.Log(LogData.Create(level, fields));
        }

        public static ILogger<T> Log<T>(this ILogger<T> logger, LogLevel level, ILogField field, params ILogField[] fields)
            where T : class {
            return logger.Log(LogData.Create(level, field, fields));
        }

        public static ILogger<T> Log<T>(this ILogger<T> logger, LogLevel level, (string name, object value) field, params (string name, object value)[] fields)
            where T : class {
            return logger.Log(LogData.Create(level, field, fields));
        }

        #region

        public static ILogger<T> Trace<T>(this ILogger<T> logger, IEnumerable<ILogField> fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Trace, fields));
        }

        public static ILogger<T> Info<T>(this ILogger<T> logger, IEnumerable<ILogField> fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Info, fields));
        }

        public static ILogger<T> Debug<T>(this ILogger<T> logger, IEnumerable<ILogField> fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Debug, fields));
        }

        public static ILogger<T> Warn<T>(this ILogger<T> logger, IEnumerable<ILogField> fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Warn, fields));
        }

        public static ILogger<T> Error<T>(this ILogger<T> logger, IEnumerable<ILogField> fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Error, fields));
        }

        #endregion

        #region

        public static ILogger<T> Trace<T>(this ILogger<T> logger, ILogField field, params ILogField[] fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Trace, field, fields));
        }

        public static ILogger<T> Info<T>(this ILogger<T> logger, ILogField field, params ILogField[] fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Info, field, fields));
        }

        public static ILogger<T> Debug<T>(this ILogger<T> logger, ILogField field, params ILogField[] fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Debug, field, fields));
        }

        public static ILogger<T> Warn<T>(this ILogger<T> logger, ILogField field, params ILogField[] fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Warn, field, fields));
        }

        public static ILogger<T> Error<T>(this ILogger<T> logger, ILogField field, params ILogField[] fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Error, field, fields));
        }

        #endregion

        #region

        public static ILogger<T> Trace<T>(this ILogger<T> logger, (string name, object value) field, params (string name, object value)[] fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Trace, field, fields));
        }

        public static ILogger<T> Info<T>(this ILogger<T> logger, (string name, object value) field, params (string name, object value)[] fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Info, field, fields));
        }

        public static ILogger<T> Debug<T>(this ILogger<T> logger, (string name, object value) field, params (string name, object value)[] fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Debug, field, fields));
        }

        public static ILogger<T> Warn<T>(this ILogger<T> logger, (string name, object value) field, params (string name, object value)[] fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Warn, field, fields));
        }

        public static ILogger<T> Error<T>(this ILogger<T> logger, (string name, object value) field, params (string name, object value)[] fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Error, field, fields));
        }

        #endregion

        #region

        public static ILogger<T> Trace<T>(this ILogger<T> logger, string msg, params (string name, object value)[] fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Trace, ("msg", msg), fields));
        }

        public static ILogger<T> Info<T>(this ILogger<T> logger, string msg, params (string name, object value)[] fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Info, ("msg", msg), fields));
        }

        public static ILogger<T> Debug<T>(this ILogger<T> logger, string msg, params (string name, object value)[] fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Debug, ("msg", msg), fields));
        }

        public static ILogger<T> Warn<T>(this ILogger<T> logger, string msg, params (string name, object value)[] fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Warn, ("msg", msg), fields));
        }

        public static ILogger<T> Error<T>(this ILogger<T> logger, string msg, params (string name, object value)[] fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Error, ("msg", msg), fields));
        }

        #endregion

        #region

        public static ILogger<T> Trace<T>(this ILogger<T> logger, string msg, Exception ex, params (string name, object value)[] fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Trace, ("msg", msg), ("error", ex.Message), fields));
        }

        public static ILogger<T> Info<T>(this ILogger<T> logger, string msg, Exception ex, params (string name, object value)[] fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Info, ("msg", msg), ("error", ex.Message), fields));
        }

        public static ILogger<T> Debug<T>(this ILogger<T> logger, string msg, Exception ex, params (string name, object value)[] fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Debug, ("msg", msg), ("error", ex.Message), fields));
        }

        public static ILogger<T> Warn<T>(this ILogger<T> logger, string msg, Exception ex, params (string name, object value)[] fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Warn, ("msg", msg), ("error", ex.Message), fields));
        }

        public static ILogger<T> Error<T>(this ILogger<T> logger, string msg, Exception ex, params (string name, object value)[] fields)
            where T : class {
            return logger.Log(LogData.Create(LogLevel.Error, ("msg", msg), ("error", ex.Message), fields));
        }

        #endregion
    }
}
