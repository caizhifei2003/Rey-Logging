﻿using Rey.Logging;
using Rey.Logging.FieldGenerators;

namespace Rey {
    public static class ILogBuilderExtensions {
        public static ILogBuilder LevelField(this ILogBuilder options) {
            return options.Field<LevelFieldGenerator>();
        }

        public static ILogBuilder TimeField(this ILogBuilder options) {
            return options.Field<TimeFieldGenerator>();
        }

        public static ILogBuilder ProcessIdField(this ILogBuilder options) {
            return options.Field<ProcessIdFieldGenerator>();
        }

        public static ILogBuilder ThreadIdField(this ILogBuilder options) {
            return options.Field<ThreadIdFieldGenerator>();
        }

        public static ILogBuilder FileField(this ILogBuilder options) {
            return options.Field<FileFieldGenerator>();
        }

        public static ILogBuilder ShortFileField(this ILogBuilder options) {
            return options.Field<ShortFileFieldGenerator>();
        }

        public static ILogBuilder LineField(this ILogBuilder options) {
            return options.Field<LineFieldGenerator>();
        }

        public static ILogBuilder ColumnField(this ILogBuilder options) {
            return options.Field<ColumnFieldGenerator>();
        }

        public static ILogBuilder MethodField(this ILogBuilder options) {
            return options.Field<MethodFieldGenerator>();
        }

        public static ILogBuilder ShortMethodField(this ILogBuilder options) {
            return options.Field<ShortMethodFieldGenerator>();
        }

        public static ILogBuilder ClassField(this ILogBuilder options) {
            return options.Field<ClassFieldGenerator>();
        }

        public static ILogBuilder ShortClassField(this ILogBuilder options) {
            return options.Field<ShortClassFieldGenerator>();
        }

        public static ILogBuilder CommonFields(this ILogBuilder options) {
            return options
                .LevelField()
                .TimeField()
                .ProcessIdField()
                .ThreadIdField();
        }

        public static ILogBuilder StackFields(this ILogBuilder options) {
            return options
                .LevelField()
                .FileField()
                .LineField()
                .ColumnField()
                .MethodField()
                .ClassField();
        }

        public static ILogBuilder ShortStackFields(this ILogBuilder options) {
            return options
                .LevelField()
                .ShortFileField()
                .LineField()
                .ColumnField()
                .ShortMethodField()
                .ShortClassField();
        }

        public static ILogBuilder AllFields(this ILogBuilder options) {
            return options
                .CommonFields()
                .StackFields();
        }

        public static ILogBuilder ShortAllFields(this ILogBuilder options) {
            return options
                .CommonFields()
                .ShortStackFields();
        }
    }
}
