﻿using System.Collections.Generic;
using System.Linq;

namespace Rey.Logging {
    public class LogData : ILogData {
        private readonly List<ILogField> fields = new List<ILogField>();
        public LogLevel Level { get; }
        public IEnumerable<ILogField> Fields => fields;

        public LogData(LogLevel level, IEnumerable<ILogField> fields) {
            this.Level = level;
            this.fields.AddRange(fields);
        }

        public ILogData Append(ILogField field) {
            this.fields.Add(field);
            return this;
        }
        public ILogData Append(IEnumerable<ILogField> fields) {
            this.fields.AddRange(fields);
            return this;
        }

        public ILogData Prepend(ILogField field) {
            this.fields.Insert(0, field);
            return this;
        }

        public ILogData Prepend(IEnumerable<ILogField> fields) {
            fields.Reverse().ToList().ForEach(field => this.fields.Insert(0, field));
            return this;
        }

        public static LogData Create(LogLevel level, IEnumerable<ILogField> fields) {
            return new LogData(level, fields);
        }

        public static LogData Create(LogLevel level, ILogField field, params ILogField[] fields) {
            return new LogData(level, MergeFields(field, fields));
        }

        public static LogData Create(LogLevel level, ILogField field1, ILogField field2, params ILogField[] fields) {
            return new LogData(level, MergeFields(field1, field2, fields));
        }

        public static LogData Create(LogLevel level, (string name, object value) field, params (string name, object value)[] fields) {
            return new LogData(level, MergeFields(field, fields));
        }

        public static LogData Create(LogLevel level, (string name, object value) field1, (string name, object value) field2, params (string name, object value)[] fields) {
            return new LogData(level, MergeFields(field1, field2, fields));
        }

        public static IEnumerable<ILogField> MergeFields(ILogField field, params ILogField[] fields) {
            var list = new List<ILogField>() { field };
            list.AddRange(fields);
            return list;
        }

        public static IEnumerable<ILogField> MergeFields(ILogField field1, ILogField field2, params ILogField[] fields) {
            var list = new List<ILogField>() { field1, field2 };
            list.AddRange(fields);
            return list;
        }

        public static IEnumerable<ILogField> MergeFields((string name, object value) field, params (string name, object value)[] fields) {
            var list = new List<ILogField>() { new LogField(field.name, field.value) };
            list.AddRange(fields.Select(x => new LogField(x.name, x.value)));
            return list;
        }

        public static IEnumerable<ILogField> MergeFields((string name, object value) field1, (string name, object value) field2, params (string name, object value)[] fields) {
            var list = new List<ILogField>() { new LogField(field1.name, field1.value), new LogField(field2.name, field2.value) };
            list.AddRange(fields.Select(x => new LogField(x.name, x.value)));
            return list;
        }
    }
}
