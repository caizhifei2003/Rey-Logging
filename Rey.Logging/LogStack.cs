﻿using System;
using System.Diagnostics;
using System.Reflection;

namespace Rey.Logging {
    public class LogStack<T> : ILogStack where T : class {
        private StackTrace Trace { get; }
        public LogStack() {
            this.Trace = new StackTrace(true);
        }

        public string GetFileName() {
            return this.FindFrame().GetFileName();
        }

        public int GetLine() {
            return this.FindFrame().GetFileLineNumber();
        }

        public int GetColumn() {
            return this.FindFrame().GetFileColumnNumber();
        }

        public MethodBase GetMethod() {
            return this.FindFrame().GetMethod();
        }

        public Type GetClass() {
            return typeof(T);
        }

        private StackFrame FindFrame() {
            var type = typeof(T);
            var frames = this.Trace.GetFrames();
            foreach (var frame in frames) {
                var method = frame.GetMethod();
                var dt = method.DeclaringType;
                while (dt != null) {
                    if (dt == type) {
                        return frame;
                    }
                    dt = dt.DeclaringType;
                }
            }
            return null;
        }
    }
}
