﻿namespace Rey.Logging {
    public class EmptyLogGroup<T> : ILogGroup<T>
        where T : class {
        public ILogGroup<T> Log(ILogData data) {
            return this;
        }

        public void Dispose() {
        }

        public static EmptyLogGroup<T> Instance { get; } = new EmptyLogGroup<T>();
    }
}
