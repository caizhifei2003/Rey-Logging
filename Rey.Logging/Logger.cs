﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rey.Logging {
    public class Logger<T> : ILogger<T>
        where T : class {
        private IEnumerable<ILogWriter> Writers { get; }
        private IEnumerable<ILogGroupWriter> GroupWriters { get; }
        private IEnumerable<ILogFilter> Filters { get; }
        private IEnumerable<ILogFieldGenerator> Fields { get; }

        public Logger(
            IEnumerable<ILogWriter> writers = null,
            IEnumerable<ILogGroupWriter> groupWriters = null,
            IEnumerable<ILogFilter> filters = null,
            IEnumerable<ILogFieldGenerator> fields = null) {
            this.Writers = writers;
            this.GroupWriters = groupWriters;
            this.Filters = filters;
            this.Fields = fields;
        }

        public ILogger<T> Log(ILogData data) {
            if (this.Writers == null || this.Writers.Count() == 0)
                return this;

            if (data == null)
                throw new ArgumentNullException(nameof(data));

            if (!this.Filter(data))
                return this;

            this.Write(this.Merge(data));
            return this;
        }

        public ILogGroup<T> Group(string name) {
            if (this.GroupWriters == null || this.GroupWriters.Count() == 0)
                return EmptyLogGroup<T>.Instance;

            return new LogGroup<T>(name, this);
        }

        internal bool Filter(ILogData data) {
            if (this.Filters == null || this.Filters.Count() == 0)
                return true;

            return !this.Filters.Any(filter => !filter.Filter(data));
        }

        internal ILogData Merge(ILogData data) {
            if (this.Fields == null || this.Fields.Count() == 0)
                return data;

            var fields = this.Fields
                .Distinct(LogFieldGeneratorComparer.Instance)
                .Select(x => x.Generate(data, new LogStack<T>()));

            return new LogData(data.Level, fields.Union(data.Fields));
        }

        internal void Write(ILogData data) {
            if (this.Writers == null || this.Writers.Count() == 0)
                return;

            foreach (var writer in this.Writers) {
                writer.WriteAsync(data);
            }
        }

        internal void WriteGroup(string name, IEnumerable<ILogData> dataList) {
            if (this.GroupWriters == null || this.GroupWriters.Count() == 0)
                return;

            if (dataList.Count() == 0)
                return;

            foreach (var writer in this.GroupWriters) {
                writer.WriteAsync(name, dataList);
            }
        }
    }
}
