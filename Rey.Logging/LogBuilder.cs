﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Rey.Logging {
    public class LogBuilder : ILogBuilder {
        private IServiceCollection Services { get; }

        public LogBuilder(IServiceCollection services) {
            this.Services = services;
        }

        public ILogBuilder Use(Action<IServiceCollection> configure) {
            if (configure == null)
                throw new ArgumentNullException(nameof(configure));

            configure(this.Services);
            return this;
        }

        public ILogBuilder Filter<T>()
            where T : class, ILogFilter {
            this.Services.AddTransient<ILogFilter, T>();
            return this;
        }

        public ILogBuilder Field<T>()
            where T : class, ILogFieldGenerator {
            this.Services.AddTransient<ILogFieldGenerator, T>();
            return this;
        }
    }
}
