﻿using System;
using System.Collections.Generic;

namespace Rey.Logging {
    public class LogGroup<T> : ILogGroup<T>
        where T : class {
        private string Name { get; }
        private Logger<T> Logger { get; }
        private List<ILogData> DataList { get; } = new List<ILogData>();

        public LogGroup(string name, Logger<T> logger) {
            this.Name = name;
            this.Logger = logger;
        }

        public ILogGroup<T> Log(ILogData data) {
            if (data == null)
                throw new ArgumentNullException(nameof(data));

            if (!this.Logger.Filter(data))
                return this;

            this.DataList.Add(this.Logger.Merge(data));
            return this;
        }

        public void Dispose() {
            this.Logger.WriteGroup(this.Name, this.DataList);
        }
    }
}
