﻿namespace Rey.Logging {
    public class LogField : ILogField {
        public string Name { get; }
        public object Value { get; }

        public LogField(string name, object value) {
            this.Name = name;
            this.Value = value;
        }

        public LogField(object value) {
            this.Value = value;
        }
    }
}
