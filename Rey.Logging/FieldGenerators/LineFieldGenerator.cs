﻿namespace Rey.Logging.FieldGenerators {
    public class LineFieldGenerator : ILogFieldGenerator {
        public ILogField Generate(ILogData data, ILogStack stack) {
            return new LogField("line", stack.GetLine());
        }
    }
}
