﻿namespace Rey.Logging.FieldGenerators {
    public class ColumnFieldGenerator : ILogFieldGenerator {
        public ILogField Generate(ILogData data, ILogStack stack) {
            return new LogField("column", stack.GetColumn());
        }
    }
}
