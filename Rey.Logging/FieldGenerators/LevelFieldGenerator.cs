﻿namespace Rey.Logging.FieldGenerators {
    public class LevelFieldGenerator : ILogFieldGenerator {
        public ILogField Generate(ILogData data, ILogStack stack) {
            return new LogField(string.Format("{0,5}", data.Level.ToString().ToLower()));
        }
    }
}
