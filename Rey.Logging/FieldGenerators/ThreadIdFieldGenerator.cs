﻿using System.Threading;

namespace Rey.Logging.FieldGenerators {
    public class ThreadIdFieldGenerator : ILogFieldGenerator {
        public ILogField Generate(ILogData data, ILogStack stack) {
            return new LogField("tid", Thread.CurrentThread.ManagedThreadId);
        }
    }
}
