﻿namespace Rey.Logging.FieldGenerators {
    public class ShortMethodFieldGenerator : ILogFieldGenerator {
        public ILogField Generate(ILogData data, ILogStack stack) {
            return new LogField("method", stack.GetMethod().Name);
        }
    }
}
