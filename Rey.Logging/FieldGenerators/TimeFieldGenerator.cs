﻿using System;

namespace Rey.Logging.FieldGenerators {
    public class TimeFieldGenerator : ILogFieldGenerator {
        public ILogField Generate(ILogData data, ILogStack stack) {
            return new LogField("time", DateTime.Now);
        }
    }
}
