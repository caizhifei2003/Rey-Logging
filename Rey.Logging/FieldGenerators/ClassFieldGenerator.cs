﻿namespace Rey.Logging.FieldGenerators {
    public class ClassFieldGenerator : ILogFieldGenerator {
        public ILogField Generate(ILogData data, ILogStack stack) {
            return new LogField("class", stack.GetClass().FullName);
        }
    }
}
