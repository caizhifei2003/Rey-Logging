﻿namespace Rey.Logging.FieldGenerators {
    public class FileFieldGenerator : ILogFieldGenerator {
        public ILogField Generate(ILogData data, ILogStack stack) {
            return new LogField("file", stack.GetFileName());
        }
    }
}
