﻿namespace Rey.Logging.FieldGenerators {
    public class MethodFieldGenerator : ILogFieldGenerator {
        public ILogField Generate(ILogData data, ILogStack stack) {
            return new LogField("method", stack.GetMethod());
        }
    }
}
