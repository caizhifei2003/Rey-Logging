﻿using System.IO;

namespace Rey.Logging.FieldGenerators {
    public class ShortFileFieldGenerator : ILogFieldGenerator {
        public ILogField Generate(ILogData data, ILogStack stack) {
            return new LogField("file", Path.GetFileName(stack.GetFileName()));
        }
    }
}
