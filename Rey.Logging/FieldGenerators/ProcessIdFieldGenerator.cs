﻿using System.Diagnostics;

namespace Rey.Logging.FieldGenerators {
    public class ProcessIdFieldGenerator : ILogFieldGenerator {
        public ILogField Generate(ILogData data, ILogStack stack) {
            return new LogField("pid", Process.GetCurrentProcess().Id);
        }
    }
}
