﻿namespace Rey.Logging.FieldGenerators {
    public class ShortClassFieldGenerator : ILogFieldGenerator {
        public ILogField Generate(ILogData data, ILogStack stack) {
            return new LogField("class", stack.GetClass().Name);
        }
    }
}
