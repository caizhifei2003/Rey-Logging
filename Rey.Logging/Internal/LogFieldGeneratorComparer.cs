﻿using System.Collections.Generic;

namespace Rey.Logging {
    internal class LogFieldGeneratorComparer : IEqualityComparer<ILogFieldGenerator> {
        public bool Equals(ILogFieldGenerator x, ILogFieldGenerator y) {
            return x.GetType().Equals(y.GetType());
        }

        public int GetHashCode(ILogFieldGenerator obj) {
            return obj.GetType().GetHashCode();
        }

        public static LogFieldGeneratorComparer Instance { get; } = new LogFieldGeneratorComparer();
    }
}
